#!/usr/bin/env python
# coding: utf-8

# # Exceptionnel event 
# ## Process start
# ## overwrite exisitng data every 30 min 
# #### Audit mecanism set but kept desactivated to increase the performance
# ##### Ready to Go!!!

# In[2]:


APPLICATION_NAME = "Event"
schema_name ="event"
table_name_1 = "evt_exceptionnel"
table_name_1 = "dmt_evt_exceptionnel"


# In[3]:


import unicodedata
import pandas as pd
# import datetime
# from pyspark.sql.types import StringType,re,time,datetime,IntegerType,DateType,FloatType,DoubleType,StructType , StructField, TimestampType
# from pyspark.sql import functions as F


# In[4]:


# Init (Don't modify this code, it is mandatory for both Development and Production modes)
import os, sys
def delPyc(pycFile):
    os.remove(pycFile + ".pyc") if os.path.exists(pycFile + ".pyc") else None
try: APPLICATION_HOME = os.environ["APPLICATION_HOME"]
except: APPLICATION_HOME = "../"
delPyc(APPLICATION_HOME + "/lib/SparkInit")
sys.path.append(os.path.abspath(APPLICATION_HOME + "/lib"))
from SparkInit import *
try: spark
except: spark = None
try: sc
except: sc = None
sparkInit = SparkInit(spark, sc, APPLICATION_HOME, DEFAULT_APPLICATION_NAME=APPLICATION_NAME)
spark = sparkInit.spark
sc = sparkInit.sc


# In[ ]:


# Show title if it's not Dev mode
if not sparkInit.DEV_MODE :
    print ("")
    print ("###########################################################")
    print ("#               Start loading evt_exceptionnel            #")
    print ("#                                                         #")
    print ("###########################################################")
    print ("")


# In[3]:


## Added By AC (Copy past from The other notebook)
table_name = "evt_exceptionnel"
evt = spark.read.format('jdbc').options(driver="com.ibm.as400.access.AS400JDBCDriver",         url="jdbc:as400://QTCPS5/EDDP1ASSTR",         user="YCARBONN",         password="y201811n",         dbtable="(select EDCDOS as reference_dossier, EDCRDO as code_responsable, EDCOSI as code_site_traitant, EDCCLI as code_client, EDCCTR as code_formule,                 EDCSOC as numero_de_societaire, EDCCSI as code_detail_cause_intervention, EDCPYS as code_pays_survenance, EDCRES as code_region_survenance, EDNUSU as ville_survenance,                 EDDSUS*100+EDDSUA as annee_survenance, EDDSUM as mois_survenance, EDDSUJ as jour_survenance, EDHSUH as heure_de_survenance, EDHSUM as minute_survenance,                 EDCSDO as situation_dossier, EDLCSS*100+EDLCEA as annee_ouverture, EDLCEM as mois_ouverture, EDLCEJ as jour_ouverture,                EDLCEH as heure_ouverture, EDLCEX as minute_ouverture, EDLMSS*100+EDLMEA as annee_maj, EDLMEM as mois_maj, EDLMEJ as jour_maj, EDLMEH as heure_maj,                 EDLMEX as minute_maj, /* MVCDOS as reference_dossier, */ MVCTMI as type_mission_veh, MVLPDI as effet_client, /* MHCDOS as reference_dossier, */ MHCTMI as type_mission_hab,                 trim(MHLPR1) ||' '|| trim(MHLPR2) ||' '||   trim(MHLPR3) ||' '|| trim(MHLPR4) ||' '|| trim(MHLPR5) ||' '|| trim(MHLPR6) ||' '|| trim(MHLPR7) as effet_client_hab                 from eddp1asstr.adosap dos left outer join eddp1asstr.amvehp veh on edcdos=mvcdos  left outer join eddp1asstr.amhabp hab on edcdos=mhcdos                 where edcrdo='EVT' and edlcea>=19 and edlcss=20) evt_exceptionnels")        .load()
evt.write.option("compression", "snappy").saveAsTable("evenements_exceptionnels.evt_exceptionnel", format = 'orc', mode = "overwrite")


# In[ ]:


# Show title if it's not Dev mode
if not sparkInit.DEV_MODE :
    print ("")
    print ("###########################################################")
    print ("#        End  loading evt_exceptionnel                    #")
    print ("#                                                         #")
    print ("###########################################################")
    print ("")


# In[ ]:


if not sparkInit.DEV_MODE :
    print ("")
    print ("###########################################################")
    print ("#        start loading  Datamart evt_exceptionnel         #")
    print ("#                                                         #")
    print ("###########################################################")
    print ("")


# In[5]:


table_name = "dmt_evt_exceptionnel"

evt_temp = spark.sql("select evt.*, dci.label_detail_cause_intervention, cli.label_client, for.label_formule, reg.label_region, lien.id_type_assistance, typa.label_type_assistance from crecorbet.evt_exceptionnel evt left outer join referentiel.code_detail_cause_intervention dci on evt.code_detail_cause_intervention=dci.id_detail_cause_intervention left outer join referentiel.code_client_ima_france cli on trim(evt.code_client)=cli.id_client left outer join referentiel.code_formule_france for on evt.code_formule=for.id_formule left outer join referentiel.code_region reg on evt.code_region_survenance=reg.id_region_ima and evt.code_pays_survenance=reg.id_pays_ima left outer join referentiel.lien_detail_cause_int_type_ass lien on dci.id_detail_cause_intervention=lien.id_detail_cause_intervention left outer join referentiel.code_type_assistance typa on lien.id_type_assistance=typa.id_type_assistance").toPandas()

#remplacement des null par des blancs
evt_temp.fillna("", inplace=True)

# nettoyage des caractères spéciaux et passage en minuscules des noms de ville et effet client
evt_temp['VILLE_SURVENANCE_COR']=(evt_temp['VILLE_SURVENANCE'].str.lower()).str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8').str.strip().str.replace('st-','saint-').str.replace('-s-','-sur-')
evt_temp['EFFET_CLIENT_COR']=(evt_temp['EFFET_CLIENT'].str.lower()).str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8').str.strip()
evt_temp['EFFET_CLIENT_HAB_COR']=(evt_temp['EFFET_CLIENT_HAB'].str.lower()).str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8').str.strip()
evt_temp['CODE_REGION_SURVENANCE']=evt_temp['CODE_REGION_SURVENANCE'].apply(str)
# création d'une liste de ville de la Haute-Corse, elle permettra de répartir les dossiers avec code-région '20' en Haute-Corse et Corse du Sud
liste_corse=["AGHIONE","AITI","ALANDO","ALBERTACCE","ALERIA","ALGAJOLA","ALTIANI","ALZI","AMPRIANI","ANTISANTI","AREGNO","ASCO","AVAPESSA","BARBAGGIO","BARRETTALI","BASTIA","BELGODERE","BIGORNO","BIGUGLIA","BISINCHI","BORGO","BRANDO","BUSTANICO","CAGNANO","CALACUCCIA","CALENZANA","CALVI","CAMBIA","CAMPANA","CAMPI","CAMPILE","CAMPITELLO","CANALE-DI-VERDE","CANARI","CANAVAGGIA","CARCHETO-BRUSTICO","CARPINETO","CARTICASI","CASABIANCA","CASALTA","CASAMACCIOLI","CASANOVA","CASEVECCHIE","CASTELLARE-DI-CASINCA","CASTELLARE-DI-MERCURIO","CASTELLO-DI-ROSTINO","CASTIFAO","CASTIGLIONE","CASTINETA","CASTIRLA","CATERI","CENTURI","CERVIONE","CHIATRA","CHISA","CORBARA","CORSCIA","CORTE","COSTA","CROCE","CROCICCHIA","ERBAJOLO","ERONE","ERSA","FARINOLE","FAVALELLO","FELCE","FELICETO","FICAJA","FOCICCHIA","FURIANI","GALERIA","GAVIGNANO","GHISONACCIA","GHISONI","GIOCATOJO","GIUNCAGGIO","ISOLACCIO-DI-FIUMORBO","LA PORTA","LAMA","LANO","LAVATOGGIO","LENTO","L'ILE-ROUSSE","LINGUIZZETTA","LORETO-DI-CASINCA","LOZZI","LUCCIANA","LUGO-DI-NAZZA","LUMIO","LURI","MANSO","MATRA","MAUSOLEO","MAZZOLA","MERIA","MOITA","MOLTIFAO","MONACIA-D'OREZZA","MONCALE","MONTE","MONTEGROSSO","MONTICELLO","MOROSAGLIA","MORSIGLIA","MURACCIOLE","MURATO","MURO","NESSA","NOCARIO","NOCETA","NONZA","NOVALE","NOVELLA","OCCHIATANA","OGLIASTRO","OLCANI","OLETTA","OLMETA-DI-CAPOCORSO","OLMETA-DI-TUDA","OLMI-CAPPELLA","OLMO","OMESSA","ORTALE","ORTIPORIO","PALASCA","PANCHERACCIA","PARATA","PATRIMONIO","PENTA-ACQUATELLA","PENTA-DI-CASINCA","PERELLI","PERO-CASEVECCHIE","PIANELLO","PIANO","PIAZZALI","PIAZZOLE","PIEDICORTE-DI-GAGGIO","PIEDICROCE","PIEDIGRIGGIO","PIEDIPARTINO","PIE-D'OREZZA","PIETRACORBARA","PIETRA-DI-VERDE","PIETRALBA","PIETRASERENA","PIETRICAGGIO","PIETROSO","PIEVE","PIGNA","PINO","PIOBETTA","PIOGGIOLA","POGGIO-DI-NAZZA","POGGIO-DI-VENACO","POGGIO-D'OLETTA","POGGIO-MARINACCIO","POGGIO-MEZZANA","POLVEROSO","POPOLASCA","PORRI","PRATO-DI-GIOVELLINA","PRUNELLI-DI-CASACCONI","PRUNELLI-DI-FIUMORBO","PRUNO","QUERCITELLO","RAPAGGIO","RAPALE","RIVENTOSA","ROGLIANO","ROSPIGLIANI","RUSIO","RUTALI","SAINT-FLORENT","SALICETO","SAN-DAMIANO","SAN-GAVINO-D'AMPUGNANI","SAN-GAVINO-DI-FIUMORBO","SAN-GAVINO-DI-TENDA","SAN-GIOVANNI-DI-MORIANI","SAN-GIULIANO","SAN-LORENZO","SAN-MARTINO-DI-LOTA","SAN-NICOLAO","SANTA-LUCIA-DI-MERCURIO","SANTA-LUCIA-DI-MORIANI","SANTA-MARIA-DI-LOTA","SANTA-MARIA-POGGIO","SANT'ANDREA-DI-BOZIO","SANT'ANDREA-DI-COTONE","SANT'ANTONINO","SANTA-REPARATA-DI-BALAGNA","SANTA-REPARATA-DI-MORIANI","SANTO-PIETRO-DI-TENDA","SANTO-PIETRO-DI-VENACO","SCATA","SCOLCA","SERMANO","SERRA-DI-FIUMORBO","SILVARECCIO","SISCO","SOLARO","SORBO-OCAGNANO","SORIO","SOVERIA","SPELONCATO","STAZZONA","TAGLIO-ISOLACCIO","TALASANI","TALLONE","TARRANO","TOMINO","TOX","TRALONCA","URTACA","VALLECALLE","VALLE-D'ALESANI","VALLE-DI-CAMPOLORO","VALLE-DI-ROSTINO","VALLE-D'OREZZA","VALLICA","VELONE-ORNETO","VENACO","VENTISERI","VENZOLASCA","VERDESE","VESCOVATO","VEZZANI","VIGNALE","VILLE-DI-PARASO","VILLE-DI-PIETRABUGNO","VIVARIO","VOLPAJOLA","ZALANA","ZILIA","ZUANI"]

# nettoyage des caractères spéciaux dans la liste et passage des noms en minuscule
liste_corse_cor = [i.lower().encode('ascii', errors='ignore').decode('utf-8') for i in liste_corse]

#Si la région de survenance est égale à 20 et la ville est présente dans la liste_corse_cor alors on remplace le 20 par 2B
#Si la région de survenance est égale à 20 et la ville n'est présente pas dans la liste_corse_cor alors on remplace le 20 par 2A
evt_temp.loc[(evt_temp['CODE_REGION_SURVENANCE']=='20') & (evt_temp['VILLE_SURVENANCE_COR'].apply(lambda x : x in liste_corse_cor)==True), 'CODE_REGION_SURVENANCE']='2B'
evt_temp.loc[(evt_temp['CODE_REGION_SURVENANCE']=='20') & (evt_temp['VILLE_SURVENANCE_COR'].apply(lambda x : x in liste_corse_cor)==False), 'CODE_REGION_SURVENANCE']='2A'

#Création d'une seule colonne Effet_CLient_VEH_HAB en concaténant l'effet client de la mission véhicule et l'effet client de la mission habitation
evt_temp['EFFET_CLIENT_VEH_HAB']=evt_temp['EFFET_CLIENT_COR']+' '+evt_temp['EFFET_CLIENT_HAB_COR']
#Création d'une seule colonne Type de mission en concaténant le type de mission "véhicule" et le type de mission "habitation"
evt_temp['TYPE_MISSION']=evt_temp['TYPE_MISSION_VEH']+' '+evt_temp['TYPE_MISSION_HAB']
evt_temp['TYPE_MISSION']= evt_temp['TYPE_MISSION'].replace([' H', 'V ',' '], ['Habitation','Véhicule','Autre'])

#Reconstitution de la date-heure d'ouverture
evt_temp['DATE_OUVERTURE']=(evt_temp['ANNEE_OUVERTURE']*10000+evt_temp['MOIS_OUVERTURE']*100+evt_temp['JOUR_OUVERTURE']).apply(str)
evt_temp['HM_OUVERTURE']=(evt_temp['HEURE_OUVERTURE']*100+evt_temp['HEURE_OUVERTURE']).apply(str).str.rjust(4,'0')
evt_temp['DATE_HEURE_OUVERTURE']=evt_temp['DATE_OUVERTURE']+evt_temp['HM_OUVERTURE']
evt_temp['DATE_OUVERTURE']=pd.to_datetime(evt_temp['DATE_OUVERTURE'],format='%Y%m%d')
evt_temp['HM_OUVERTURE'] = pd.to_datetime(evt_temp['HM_OUVERTURE'],format= '%H%M' )
evt_temp['DATE_HEURE_OUVERTURE']=pd.to_datetime(evt_temp['DATE_HEURE_OUVERTURE'],format='%Y%m%d%H%M')

# sauvegarde des données précalculées dans Hive
spark_df = spark.createDataFrame(evt_temp)
spark_df.write.option("compression", "snappy").saveAsTable("evenements_exceptionnels.dmt_evt_exceptionnel", format = 'orc', mode = "overwrite")


# In[6]:


# Show title if it's not Dev mode
if not sparkInit.DEV_MODE :
    print ("")
    print ("###########################################################")
    print ("#        End loading  Datamart evt_exceptionnel           #")
    print ("#                                                         #")
    print ("###########################################################")
    print ("")

