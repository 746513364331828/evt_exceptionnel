[15:02] Abdelbarre CHAFIK
    
#!/bin/bash


###############################################
# Recherche intelligente du Home du composant #
###############################################
SCRIPT_FILE=$0
SCRIPT_DIR=`(cd \`dirname ${SCRIPT_FILE}\`; pwd)`
# If the script file is a symbolic link
if [[ -L "${SCRIPT_FILE}" ]]
then
    SCRIPT_FILE=`ls -la ${SCRIPT_FILE} | cut -d">" -f2`
    SCRIPT_DIR=`(cd \`dirname ${SCRIPT_FILE}\`; pwd)`
fi
# Remove the '..' in the path (if using a link or a source)
export APPLICATION_HOME="$(dirname "${SCRIPT_DIR}")"


###########################
# Chargement de la config #
###########################
set -o allexport
. ${APPLICATION_HOME}/conf/application.conf
set +o allexport
# --------------------------------------------------------------------------


# The job file name
JOB_FILE_NAME=$1


# Create a temp directory for Spark warehouse
#SPARK_WAREHOUSE_DIR=`mktemp -d`


#export _PYSPARK_DRIVER_CONN_INFO_PATH="${SPARK_WAREHOUSE_DIR}/conn"


# List of jdbc libraries
if [ -d "${APPLICATION_HOME}/lib/jdbc" ]; then
    JDBC_LIST=$(JARS=(${APPLICATION_HOME}/lib/jdbc/*.jar); IFS=,; echo "${JARS[*]}")
fi


# List of jars libraries
if [ -d "${APPLICATION_HOME}/lib/jar" ]; then
    export JAR_LIST=$(JARS=(${APPLICATION_HOME}/lib/jar/*.jar); IFS=,; echo "${JARS[*]}")
    if [ ! "${JDBC_LIST}" = "" ]; then
        JAR_LIST="${JAR_LIST},${JDBC_LIST}"
    fi
fi


# Default jdbc librairies
if [ "${JDBC_LIST}" = "" ]; then
    JDBC_LIST="${APPLICATION_HOME}/lib/jdbc/*.jar"
fi


# Default jar librairies
if [ "${JAR_LIST}" = "" ]; then
    JAR_LIST="${APPLICATION_HOME}/lib/jar/*.jar"
fi


# Show the list of jar libraries
echo "JAR_LIST:"
echo "--jars ${JAR_LIST}"
echo


# Show the list of jdbc libraries
echo "JDBC_LIST:"
echo "--driver-class-path ${JDBC_LIST}"
echo


# Execute the Spark job -
${SPARK_HOME}/bin/spark-submit \
    --master "yarn" \
    --deploy-mode "client" \
    --name "${APPLICATION_NAME}" \
    --conf "spark.executor.memoryOverhead=20g" \
    --conf "spark.eventLog.dir=hdfs:///spark2-history" \
    --conf "spark.eventLog.enabled=false" \
    --conf "spark.driver.memory=40g" \
    --conf "spark.driver.maxResultSize=10g" \
    --conf "spark.executor.memory=10g" \
    --conf "spark.serializer=org.apache.spark.serializer.KryoSerializer" \
    --conf "spark.memory.offHeap.enabled=true" \
    --conf "spark.memory.offHeap.size=1g" \
    --conf "spark.io.compression.codec=snappy" \
    --conf "spark.dynamicAllocation.enabled=true" \
    --conf "spark.dynamicAllocation.initialExecutors=6" \
    --conf "spark.dynamicAllocation.maxExecutors=12" \
    --conf "spark.dynamicAllocation.minExecutors=6" \
    --conf "spark.memory.fraction=0.75" \
    --conf "spark.memory.storageFraction=0.75" \
    --conf "spark.default.parallelism=100" \
    --conf "spark.debug.maxToStringFields=250" \
    --conf "spark.shuffle.service.enabled=true" \
    --conf "spark.ui.port=${SPARK_MASTER_WEBUI_PORT}" \
    --files "/etc/hive/conf.cloudera.hive/hive-site.xml" \
    --jars ${JAR_LIST} \
    --driver-class-path ${JDBC_LIST} \
    ${JOB_FILE_NAME}
#    --packages com.databricks:spark-xml_2.11:0.4.1,com.stratio.datasource:spark-mongodb_2.11:0.12.0 \
# Delete the temp directory of Spark warehouse
#--conf "spark.sql.warehouse.dir=${SPARK_WAREHOUSE_DIR}" \
#rm -rf ${SPARK_WAREHOUSE_DIR}














